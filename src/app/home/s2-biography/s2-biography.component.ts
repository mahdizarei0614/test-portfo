import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-s2-biography',
  templateUrl: './s2-biography.component.html',
  styleUrls: ['./s2-biography.component.scss']
})
export class S2BiographyComponent {
  @Input() activated = false;
}
